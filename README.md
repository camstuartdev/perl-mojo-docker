# perl-mojo-docker

A docker image with "typical" modules installed for Mojolicious apps

```
docker build . -t camstuart/perl-mojo-docker:0.1
docker push camstuart/perl-mojo-docker:0.1
```
